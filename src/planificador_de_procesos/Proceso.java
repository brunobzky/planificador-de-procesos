
package planificador_de_procesos;


public class Proceso {
    
    String nombre;
    int rafaga;
    int posicion;
    int prioridad;
    

    public Proceso(String nombre, int rafaga, int posicion, int prioridad) {
        this.nombre = nombre;
        this.rafaga = rafaga;
        this.posicion = posicion;
        this.prioridad = prioridad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setRafaga(int rafaga) {
        this.rafaga = rafaga;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }
    

    public String getNombre() {
        return nombre;
    }

    public int getRafaga() {
        return rafaga;
    }

    public int getPosicion() {
        return posicion;
    }

    public int getPrioridad() {
        return prioridad;
    }
    
}
