package interfaz;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import planificador_de_procesos.Proceso;
import java.util.Arrays;
import planificador_de_procesos.Proceso;
import java.lang.Comparable;

public class Interfaz extends javax.swing.JFrame {

    int cantidad;
    DefaultTableModel modelo2;
    ArrayList P;

    public Interfaz() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnLlenar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbDatos = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btnEjecutar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaLog = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Planificador de procesos");
        setBackground(new java.awt.Color(102, 102, 102));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnLlenar.setBackground(new java.awt.Color(0, 84, 159));
        btnLlenar.setFont(new java.awt.Font("Exo", 0, 14)); // NOI18N
        btnLlenar.setForeground(new java.awt.Color(255, 255, 255));
        btnLlenar.setText("Llenar");
        btnLlenar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnLlenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLlenarActionPerformed(evt);
            }
        });

        tbDatos.setFont(new java.awt.Font("Exo", 0, 12)); // NOI18N
        tbDatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre proceso", "Ráfagas", "Tiempo de llegada", "Prioridad"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbDatos);

        jLabel1.setFont(new java.awt.Font("Exo", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(63, 156, 53));
        jLabel1.setText("PROCESOS A EJECUTAR (MÁX 50)");

        txtCantidad.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 16)); // NOI18N

        btnEjecutar.setBackground(new java.awt.Color(0, 84, 159));
        btnEjecutar.setFont(new java.awt.Font("Exo", 0, 14)); // NOI18N
        btnEjecutar.setForeground(new java.awt.Color(255, 255, 255));
        btnEjecutar.setText("Ejecutar");
        btnEjecutar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnEjecutar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEjecutarActionPerformed(evt);
            }
        });

        txaLog.setColumns(20);
        txaLog.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 16)); // NOI18N
        txaLog.setRows(5);
        jScrollPane2.setViewportView(txaLog);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(242, 242, 242)
                        .addComponent(btnEjecutar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnLlenar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(69, 69, 69)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEjecutar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLlenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLlenarActionPerformed

        P = new ArrayList();//Arreglo donde se almacenarán los valores
        String c = txtCantidad.getText();

        try {
            cantidad = Integer.valueOf(c);//Numero de procesos a generar

            if (cantidad > 0 && cantidad <= 50) {
                for (int i = 0; i < cantidad; i++) {//Llenar la tabla con valores por default
                    Proceso p0 = new Proceso("A", 0, 0, 0);
                    P.add(p0);
                }

                Set<Integer> nombreAleatorio = new HashSet<>();//Generar el aleatorio
                for (int i = 0; i < cantidad; i++) {
                    int aleatorio = -1;
                    boolean generado = false;
                    while (!generado) {
                        int posible = ThreadLocalRandom.current().nextInt(65, 122);
                        if (!nombreAleatorio.contains(posible)) {
                            nombreAleatorio.add(posible);
                            aleatorio = posible;
                            generado = true;
                        }
                    }
                    Proceso a = (Proceso) P.get(i);
                    a.setNombre(Character.toString((char) aleatorio));//Asigna el nombre al proceso
                }

                Set<Integer> rafagaAleatoria = new HashSet<>();//Generar el aleatorio
                for (int i = 0; i < cantidad; i++) {
                    int aleatorio = -1;
                    boolean generado = false;
                    while (!generado) {
                        int posible = ThreadLocalRandom.current().nextInt(0, 50);
                        if (!rafagaAleatoria.contains(posible)) {
                            rafagaAleatoria.add(posible);
                            aleatorio = posible;
                            generado = true;
                        }
                    }
                    Proceso a = (Proceso) P.get(i);
                    a.setRafaga(aleatorio);//Asigna las rafagas que le tomará al proceso ejecutarse
                }

                CrearModelo2();//Dibujar la tabla
                Object O[] = null;//Objeto vacio
                for (int i = 0; i < P.size(); i++) {//Llenar la tabla con los datos aleatorioa
                    modelo2.addRow(O);
                    Proceso getP = (Proceso) P.get(i);
                    modelo2.setValueAt(getP.getNombre(), i, 0);
                    modelo2.setValueAt(getP.getRafaga(), i, 1);
                    modelo2.setValueAt(getP.getPosicion(), i, 2);
                    modelo2.setValueAt(getP.getPrioridad(), i, 3);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Agrega un número entre 1 y 50 al campo de texto");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Agrega un número entre 1 y 50 al campo de texto");
        }

        ArrayList T = new ArrayList();

        for (int i = 0; i < cantidad; i++) {//Llenar la tabla con valores por default
            Proceso p1 = new Proceso("A", 0, 0, 0);
            T.add(p1);
        }

        Set<Integer> posicionLlegada = new HashSet<>();
        posicionLlegada.add(0);
        for (int i = 1; i < cantidad; i++) {
            int aleatorio = -1;
            boolean generado = false;
            while (!generado) {
                try {
                    String st = JOptionPane.showInputDialog("Ingresa un número:");
                    int posible = Integer.parseInt(st);
                    if (!posicionLlegada.contains(posible)) {
                        posicionLlegada.add(posible);
                        aleatorio = posible;
                        generado = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "Este valor ya existe, ingresa uno diferente");
                    }
                } catch (Exception e) {
                    JOptionPane.showConfirmDialog(null, "Ingresa un número válido");
                }

                CrearModelo2();//Dibujar la tabla
                Object O[] = null;//Objeto vacio
                for (int i = 0; i < P.size(); i++) {//Llenar la tabla con los datos aleatorioa
                    modelo2.addRow(O);
                    Proceso getP = (Proceso) P.get(i);
                    modelo2.setValueAt(getP.getNombre(), i, 0);
                    modelo2.setValueAt(getP.getRafaga(), i, 1);
                    modelo2.setValueAt(getP.getPosicion(), i, 2);
                    modelo2.setValueAt(getP.getPrioridad(), i, 3);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Agrega un número entre 1 y 50 al campo de texto");
            }
            Proceso b = (Proceso) T.get(i);//Obtiene la fila que se llenará
            b.setPosicion(aleatorio);//Asigna la posición que tiene el proceso para ejecutarse
        }

        for (int i = 0; i < T.size(); i++) {//Llenar la tabla con los datos del arraylist
            Proceso getP = (Proceso) T.get(i);
            modelo2.setValueAt(getP.getPosicion(), i, 2);
        }

    }//GEN-LAST:event_btnLlenarActionPerformed

    private void btnEjecutarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEjecutarActionPerformed

        


    }//GEN-LAST:event_btnEjecutarActionPerformed

   @Override
        public int compareTo(Proceso o) {
            if ( o.posicion < o.getPosicion()) {
                return -1;
            }
            if (o.posicion > o.getPosicion()) {
                return 1;
            }
            return 0;
        }
        
        public void ordenar(){
             Arrays.sort(P);
        }
    
    
    private void CrearModelo2() {//Modelo de la tabla que se va a utilizar
        try {
            modelo2 = (new DefaultTableModel(
                    null, new String[]{
                        "Nombre proceso", "Ráfagas",
                        "Tiempo de llegada", "Prioridad"}) {
                Class[] types = new Class[]{
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class,
                    java.lang.Object.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int colIndex) {
                    return canEdit[colIndex];
                }
            });
            tbDatos.setModel(modelo2);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString() + "error2");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEjecutar;
    private javax.swing.JButton btnLlenar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbDatos;
    private javax.swing.JTextArea txaLog;
    private javax.swing.JTextField txtCantidad;
    // End of variables declaration//GEN-END:variables
}
