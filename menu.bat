@echo off
:menu
cls
                        
echo  1. apagar el equipo               
echo  2. Informacion de su equipo         
echo  3. Crear un proceso                 
echo  4. Eliminar un proceso              
echo  5. copiar el contenido de una USB   
echo  6. Salir    

echo ----------------------------------------------------------------------------------------------------

@CHOICE /C:123456 /M "Selecciona un numero"%
IF ERRORLEVEL 1 goto apagar
IF ERRORLEVEL 2 goto info
IF ERRORLEVEL 3 goto crear
IF ERRORLEVEL 4 goto eliminar
IF ERRORLEVEL 5 goto copiar
IF ERRORLEVEL 6 goto salir

echo hola
:apagar
cls
TITLE Apagar el equipo
SET /P seg=Introduzca los segundos para apagar el equipo: 
SHUTDOWN /s /f /t %seg%
ECHO El equipo se apagará
ECHO presione ENTER para continuar!
PAUSE>nul

:info
cls
TITLE Ver informacion del equipo!
SET /P name= introduzca el hostname: 
cd..
cd..
systeminfo
systeminfo /s %name% | find "ie6"
systeminfo /s %name% | find "KB"
systeminfo /s %name% | find "K"
systeminfo /s %name% | find "memo"
echo.
echo.
echo presione ENTER para regresar al menu principal.
PAUSE>nul
goto menu

:crear
cls
TITLE crear un proceso
SET /P pro= Introduzca el proceso a ejecutar: 
start %pro%
echo.
echo.
echo presione ENTER para regresar al menu principal.
PAUSE>nul
goto menu

:eliminar
cls
TITLE Eliminar proceso
SET /P pro= Introduzca el proceso que desea matar: 
taskkill /f /%pro%
echo.
echo.
echo presione ENTER para regresar al menu principal.
PAUSE>nul
goto menu

:copiar
cls
TITLE Copiar el contenido de una USB
SET /P cop= Introducir la ruta donde desea pegar la informacion: 
robocopy E:\ C:\%cop% /mir
robocopy F:\ C:\%cop% /mir
robocopy G:\ C:\%cop% /mir
echo.
echo.
echo presione Enter para regresar al menu principal.
PAUSE>nul
goto menu

:salir
echo presione ENTER para salir
pause>nul
exit